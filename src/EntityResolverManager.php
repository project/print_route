<?php

namespace Drupal\print_route;

use Drupal\Core\Entity\EntityResolverManager as parentClass;

/**
 * Class EntityResolverManager.
 *
 * @package Drupal\print_route
 */
class EntityResolverManager extends parentClass {

  /**
   * Gets the controller class using route defaults.
   *
   * @param array $defaults
   *   The default values provided by the route.
   *
   * @return array|string|null
   *   Returns the controller class, otherwise NULL.
   *
   * @see \Drupal\Core\Entity\EntityResolverManager::getControllerClass
   */
  public function getController(array $defaults) {
    return $this->getControllerClass($defaults);
  }

}
