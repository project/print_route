<?php

namespace Drupal\print_route;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Class NullContentEntityStorage.
 *
 * @package Drupal\print_route
 */
class NullContentEntityStorage implements ContentEntityStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function createWithSampleValues($bundle = FALSE, array $values = []) {
    // TODO: Implement createWithSampleValues() method.
  }

  /**
   * {@inheritdoc}
   */
  public function resetCache(array $ids = NULL) {
    // TODO: Implement resetCache() method.
  }

  /**
   * {@inheritdoc}
   */
  public function loadMultiple(array $ids = NULL) {
    // TODO: Implement loadMultiple() method.
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function load($id) {
    // TODO: Implement load() method.
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function loadUnchanged($id) {
    // TODO: Implement loadUnchanged() method.
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function loadRevision($revision_id) {
    // TODO: Implement loadRevision() method.
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteRevision($revision_id) {
    // TODO: Implement deleteRevision() method.
  }

  /**
   * {@inheritdoc}
   */
  public function loadByProperties(array $values = []) {
    // TODO: Implement loadByProperties() method.
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function create(array $values = []) {
    // TODO: Implement create() method.
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function delete(array $entities) {
    // TODO: Implement delete() method.
  }

  /**
   * {@inheritdoc}
   */
  public function save(EntityInterface $entity) {
    // TODO: Implement save() method.
  }

  /**
   * {@inheritdoc}
   */
  public function restore(EntityInterface $entity) {
    // TODO: Implement restore() method.
  }

  /**
   * {@inheritdoc}
   */
  public function hasData() {
    // TODO: Implement hasData() method.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuery($conjunction = 'AND') {
    // TODO: Implement getQuery() method.
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getAggregateQuery($conjunction = 'AND') {
    // TODO: Implement getAggregateQuery() method.
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeId() {
    // TODO: Implement getEntityTypeId() method.
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityType() {
    // TODO: Implement getEntityType() method.
  }

  /**
   * {@inheritdoc}
   */
  public function loadMultipleRevisions(array $revision_ids) {
    // TODO: Implement loadMultipleRevisions() method.
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getLatestRevisionId($entity_id) {
    // TODO: Implement getLatestRevisionId() method.
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function createRevision(RevisionableInterface $entity, $default = TRUE, $keep_untranslatable_fields = NULL) {
    // TODO: Implement createRevision() method.
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getLatestTranslationAffectedRevisionId($entity_id, $langcode) {
    // TODO: Implement getLatestTranslationAffectedRevisionId() method.
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function createTranslation(ContentEntityInterface $entity, $langcode, array $values = []) {
    // TODO: Implement createTranslation() method.
    return NULL;
  }

}
