<?php

namespace Drupal\print_route\Renderer;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Asset\AttachedAssets;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\ParamConverter\EntityConverter;
use Drupal\Core\Render\AttachmentsInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface as CoreRendererInterface;
use Drupal\entity_print\Asset\AssetRendererInterface;
use Drupal\entity_print\Event\PrintEvents;
use Drupal\entity_print\Event\PrintHtmlAlterEvent;
use Drupal\entity_print\FilenameGeneratorInterface;
use Drupal\print_route\EntityResolverManager;
use ReflectionFunction;
use ReflectionMethod;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\entity_print\Renderer\RendererBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Entity\EntityRepository;
use Drupal\Core\Asset\AssetResolverInterface;
use Drupal\Core\Asset\AssetCollectionRendererInterface;
use Drupal\Component\Transliteration\TransliterationInterface;

/**
 * Providers a renderer for routes.
 */
class RouteRenderer extends RendererBase implements TrustedCallbackInterface {

  /**
   * Drupal\Core\Routing\RouteProviderInterface definition.
   *
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected $routerRouteProvider;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The class resolver.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected $classResolver;

  /**
   * Entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepository
   */
  protected $entityRepository;

  /**
   * The asset resolver.
   *
   * @var \Drupal\Core\Asset\AssetResolverInterface
   */
  protected $assetResolver;

  /**
   * The css asset renderer.
   *
   * @var \Drupal\Core\Asset\CssCollectionRenderer
   */
  protected $cssRenderer;

  /**
   * The transliteration service.
   *
   * @var \Drupal\Component\Transliteration\TransliterationInterface
   */
  protected $transliteration;

  /**
   * RendererBase constructor.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   Cores renderer.
   * @param \Drupal\entity_print\Asset\AssetRendererInterface $asset_renderer
   *   The asset renderer.
   * @param \Drupal\entity_print\FilenameGeneratorInterface $filename_generator
   *   Filename generator.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\Routing\RouteProviderInterface $routerRouteProvider
   *   Drupal\Core\Routing\RouteProviderInterface definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Drupal\Core\Entity\EntityTypeManagerInterface definition.
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $classResolver
   *   The class resolver.
   * @param \Drupal\Core\Entity\EntityRepository $entityRepository
   *   Entity repository.
   * @param \Drupal\Core\Asset\AssetResolverInterface $asset_resolver
   *   The asset resolver.
   * @param \Drupal\Core\Asset\AssetCollectionRendererInterface $css_renderer
   *   The CSS renderer.
   * @param \Drupal\Component\Transliteration\TransliterationInterface $transliteration
   *   Transliteration service.
   */
  public function __construct(
    CoreRendererInterface $renderer,
    AssetRendererInterface $asset_renderer,
    FilenameGeneratorInterface $filename_generator,
    EventDispatcherInterface $event_dispatcher,
    RouteProviderInterface $routerRouteProvider,
    EntityTypeManagerInterface $entityTypeManager,
    ClassResolverInterface $classResolver,
    EntityRepository $entityRepository,
    AssetResolverInterface $asset_resolver,
    AssetCollectionRendererInterface $css_renderer,
    TransliterationInterface $transliteration
  ) {
    parent::__construct($renderer, $asset_renderer, $filename_generator, $event_dispatcher);
    $this->routerRouteProvider = $routerRouteProvider;
    $this->entityTypeManager = $entityTypeManager;
    $this->classResolver = $classResolver;
    $this->entityRepository = $entityRepository;
    $this->assetResolver = $asset_resolver;
    $this->cssRenderer = $css_renderer;
    $this->transliteration = $transliteration;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $container->get('renderer'),
      $container->get('entity_print.asset_renderer'),
      $container->get('entity_print.filename_generator'),
      $container->get('event_dispatcher'),
      $container->get('router.route_provider'),
      $container->get('entity_type.manager'),
      $container->get('class_resolver'),
      $container->get('entity.repository'),
      $container->get('asset.resolver'),
      $container->get('asset.css.collection_renderer'),
      $container->get('transliteration')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function render(array $routes) {
    return array_map([$this, 'renderSingle'], $routes);
  }

  /**
   * Render a single entity.
   *
   * @param object $printable
   *   The entity we're rendering.
   *
   * @return array
   *   A render array.
   */
  protected function renderSingle($printable) {
    // Prepare and execute the route internally.
    $params = $printable->parameters;
    $routeName = $params->get('_routename');
    $route = $this->routerRouteProvider->getRouteByName($routeName);
    $routeOptions = $route->getOptions();
    if (!isset($routeOptions['parameters'])) {
      $routeOptions['parameters'] = [];
    }
    foreach ($route->getDefaults() as $key => $value) {
      if (substr($key, 0, 1) != '_') {
        if (!$params->has($key)) {
          $params->add([$key => $value]);
        }
      }
    }

    $entityResolverManager = new EntityResolverManager($this->entityTypeManager, $this->classResolver);

    $class_or_service = $entityResolverManager->getController($route->getDefaults());
    $controllerInstance = $this->classResolver->getInstanceFromDefinition($class_or_service[0]);

    $paramConverter = new EntityConverter($this->entityTypeManager, $this->entityRepository);

    $arguments = [];
    foreach ($params->getIterator() as $key => $value) {
      if (substr($key, 0, 1) != '_') {
        if (isset($routeOptions['parameters'][$key])) {
          $arguments[$key] = $paramConverter->convert($value, $routeOptions['parameters'][$key], $key, $route->getDefaults());
        }
        else {
          $arguments[$key] = $value;
        }
      }
    }

    if (isset($class_or_service[1])) {
      $controller = [$controllerInstance, $class_or_service[1]];
    }
    else {
      $controller = [$controllerInstance];
    }

    $context = new RenderContext();
    $response = $this->renderer->executeInRenderContext($context, function () use ($controller, $arguments) {
      // Now call the actual controller, just like HttpKernel does.
      return call_user_func_array($controller, $this->sortCallParameters($controller, $arguments));
    });

    $response['#pre_render'][] = [static::class, 'preRender'];

    // If early rendering happened, i.e. if code in the controller called
    // drupal_render() outside of a render context, then the bubbleable metadata
    // for that is stored in the current render context.
    if (!$context->isEmpty()) {
      /** @var \Drupal\Core\Render\BubbleableMetadata $early_rendering_bubbleable_metadata */
      $early_rendering_bubbleable_metadata = $context->pop();

      // If a render array or AjaxResponse is returned by the controller, merge
      // the "lost" bubbleable metadata.
      if (is_array($response)) {
        BubbleableMetadata::createFromRenderArray($response)
          ->merge($early_rendering_bubbleable_metadata)
          ->applyTo($response);
      }
      elseif ($response instanceof AjaxResponse) {
        $response->addAttachments($early_rendering_bubbleable_metadata->getAttachments());
        // @todo Make AjaxResponse cacheable in
        //   https://www.drupal.org/node/956186. Meanwhile, allow contrib
        //   subclasses to be.
        if ($response instanceof CacheableResponseInterface) {
          $response->addCacheableDependency($early_rendering_bubbleable_metadata);
        }
      }
      // If a non-Ajax Response or domain object is returned and it cares about
      // attachments or cacheability, then throw an exception: early rendering
      // is not permitted in that case. It is the developer's responsibility
      // to not use early rendering.
      elseif ($response instanceof AttachmentsInterface || $response instanceof CacheableResponseInterface || $response instanceof CacheableDependencyInterface) {
        throw new \LogicException(sprintf('The controller result claims to be providing relevant cache metadata, but leaked metadata was detected. Please ensure you are not rendering content too early. Returned object class: %s.', get_class($response)));
      }
      else {
        // A Response or domain object is returned that does not care about
        // attachments nor cacheability; for instance, a RedirectResponse. It is
        // safe to discard any early rendering metadata.
      }
    }

    return $response;
  }

  /**
   * Sort parameters with the callable definition.
   *
   * @param mixed $function
   *   The function to be called.
   * @param array $param_arr
   *   The parameters to be passed to the function, as an named array.
   *
   * @return mixed
   *   Parameters sorted.
   *
   * @see https://www.php.net/manual/es/reflectionmethod.invokeargs.php#100041
   * @see https://www.drupal.org/project/drupal/issues/3174150
   */
  protected function sortCallParameters($function, array $param_arr) {
    if (count((array) $function) == 2) {
      $reflection = new ReflectionMethod($function[0], $function[1]);
    }
    else {
      $reflection = new ReflectionFunction($function[0]);
    }

    $pass = [];
    foreach ($reflection->getParameters() as $param) {
      /* @var $param ReflectionParameter */
      if (isset($param_arr[$param->getName()])) {
        $pass[] = $param_arr[$param->getName()];
      }
      else {
        $pass[] = $param->getDefaultValue();
      }
    }

    return $pass;
  }

  /**
   * {@inheritdoc}
   */
  public function generateHtml(array $entities, array $render, $use_default_css, $optimize_css) {
    // Collect content attached libraries.
    $contentAssets = $this->findContentAssets($render["#content"]);
    if (!isset($render['#attached']['library'])) {
      $render['#attached']['library'] = [];
    }
    $render['#attached']['library'] += $contentAssets;
    $css_assets = $this->assetResolver->getCssAssets(AttachedAssets::createFromRenderArray($render), $optimize_css);
    $rendered_css = $this->cssRenderer->render($css_assets);

    $rendered_css[] = $this->assetRenderer->render($entities, $use_default_css, $optimize_css);
    $render['#entity_print_css'] = $this->renderer->executeInRenderContext(new RenderContext(), function () use (&$rendered_css) {
      return $this->renderer->render($rendered_css);
    });

    $html = (string) $this->renderer->executeInRenderContext(new RenderContext(), function () use (&$render) {
      return $this->renderer->render($render);
    });

    // Allow other modules to alter the generated HTML.
    $this->dispatcher->dispatch(PrintEvents::POST_RENDER, new PrintHtmlAlterEvent($html, $entities));

    return $html;
  }

  /**
   * Find the content assets.
   *
   * @param array $content
   *   Content.
   *
   * @return array|null
   *   Asset or NULL.
   */
  protected function findContentAssets(array $content) {
    foreach ($content as $key => $element) {
      if ($key === '#attached') {
        if (isset($element['library'])) {
          return $element['library'];
        }
        return NULL;
      }
      elseif (is_array($element)) {
        $resp = $this->findContentAssets($element);
        if ($resp) {
          return $resp;
        }
      }
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getFilename(array $entities) {
    // Get title and use how file name.
    $entity = current($entities);
    return trim($this->sanitizeFilename($entity->label(), $entity->language()->getId()));
  }

  /**
   * Gets a safe filename.
   *
   * @param string $filename
   *   The un-processed filename.
   * @param string $langcode
   *   The language of the filename.
   *
   * @return string
   *   The filename stripped to only safe characters.
   */
  protected function sanitizeFilename($filename, $langcode) {
    $transformed = $this->transliteration->transliterate($filename, $langcode);
    return preg_replace("/[^A-Za-z0-9 ]/", '', $transformed);
  }

  /**
   * Pre render callback for the view.
   */
  public static function preRender(array $element) {
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['preRender'];
  }

}
