<?php

namespace Drupal\print_route\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\ParamConverter\EntityConverter;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Entity\EntityStorageException;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Class PrintableRouteEntity.
 *
 * @package Drupal\print_route\Entity
 *
 * @ContentEntityType(
 *   id = "printable_route",
 *   handlers = {
 *     "entity_print" = "Drupal\print_route\Renderer\RouteRenderer",
 *     "storage" = "Drupal\print_route\NullContentEntityStorage",
 *     "view_builder" = "",
 *   },
 * )
 */
class PrintableRouteEntity implements EntityInterface {

  /**
   * Build info.
   *
   * @var array
   */
  protected $buildInfo = [];

  /**
   * Magic properties get.
   *
   * @param string $name
   *   Property name.
   *
   * @return mixed|null
   *   Property value or NULL.
   */
  public function __get($name) {
    if (!isset($this->buildInfo[$name])) {
      return NULL;
    }
    return $this->buildInfo[$name];
  }

  /**
   * {@inheritdoc}
   */
  public function access($operation, AccountInterface $account = NULL, $return_as_object = FALSE) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    // TODO: Implement getCacheContexts() method.
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    // TODO: Implement getCacheTags() method.
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    // TODO: Implement getCacheMaxAge() method.
  }

  /**
   * {@inheritdoc}
   */
  public function uuid() {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function id() {
    return uniqid();
  }

  /**
   * {@inheritdoc}
   */
  public function language() {
    return \Drupal::languageManager()->getDefaultLanguage();
  }

  /**
   * {@inheritdoc}
   */
  public function isNew() {
    // TODO: Implement isNew() method.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function enforceIsNew($value = TRUE) {
    // TODO: Implement enforceIsNew() method.
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeId() {
    return 'printable_route';
  }

  /**
   * {@inheritdoc}
   */
  public function bundle() {
    return 'printable_route';
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    $query = [];
    $routeName = NULL;
    foreach ($this->buildInfo['parameters'] as $name => $value) {
      if (substr($name, 0, 1) != '_') {
        $query[$name] = $value;
      }
      elseif ($name == '_routename') {
        $routeName = $value;
      }
    }

    /** @var \Drupal\Core\Routing\RouteProvider $routerProvider */
    $routerProvider = \Drupal::service('router.route_provider');

    /** @var \Symfony\Component\Routing\Route $route */
    $route = $routerProvider->getRouteByName($routeName);

    $paramConverter = new EntityConverter(\Drupal::entityTypeManager(), \Drupal::service('class_resolver'));
    $routeOptions = $route->getOptions();
    $arguments = [];
    foreach ($query as $key => $value) {
      if (substr($key, 0, 1) != '_') {
        if (isset($routeOptions['parameters'][$key])) {
          $arguments[$key] = $paramConverter->convert($value, $routeOptions['parameters'][$key], $key, $route->getDefaults());
        }
        else {
          $arguments[$key] = $value;
        }
      }
    }

    $request = new Request([], [], $arguments);
    $title = \Drupal::service('title_resolver')->getTitle($request, $route);
    if ($title) {
      if ($title instanceof TranslatableMarkup) {
        /** @var \Drupal\Core\StringTranslation\TranslatableMarkup $title */
        $title = strip_tags($title->render());
      }
      return $title;
    }

    return 'Document';
  }

  /**
   * {@inheritdoc}
   */
  public function urlInfo($rel = 'canonical', array $options = []) {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function toUrl($rel = 'canonical', array $options = []) {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function url($rel = 'canonical', $options = []) {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function link($text = NULL, $rel = 'canonical', array $options = []) {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function toLink($text = NULL, $rel = 'canonical', array $options = []) {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function hasLinkTemplate($key) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function uriRelationships() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public static function load($id) {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function loadMultiple(array $ids = NULL) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(array $values = []) {
    $instance = new static();
    $instance->buildInfo = $values;
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function save() {
    throw new EntityStorageException();
  }

  /**
   * {@inheritdoc}
   */
  public function delete() {
    throw new EntityStorageException();
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    // TODO: Implement preSave() method.
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    // TODO: Implement postSave() method.
  }

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values) {
    // TODO: Implement preCreate() method.
  }

  /**
   * {@inheritdoc}
   */
  public function postCreate(EntityStorageInterface $storage) {
    // TODO: Implement postCreate() method.
  }

  /**
   * {@inheritdoc}
   */
  public static function preDelete(EntityStorageInterface $storage, array $entities) {
    // TODO: Implement preDelete() method.
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    // TODO: Implement postDelete() method.
  }

  /**
   * {@inheritdoc}
   */
  public static function postLoad(EntityStorageInterface $storage, array &$entities) {
    // TODO: Implement postLoad() method.
  }

  /**
   * {@inheritdoc}
   */
  public function createDuplicate() {
    return static::create($this->buildInfo);
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityType() {
    // TODO: Implement getEntityType() method.
  }

  /**
   * {@inheritdoc}
   */
  public function referencedEntities() {
    // TODO: Implement referencedEntities() method.
  }

  /**
   * {@inheritdoc}
   */
  public function getOriginalId() {
    // TODO: Implement getOriginalId() method.
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTagsToInvalidate() {
    // TODO: Implement getCacheTagsToInvalidate() method.
  }

  /**
   * {@inheritdoc}
   */
  public function setOriginalId($id) {
    // TODO: Implement setOriginalId() method.
  }

  /**
   * {@inheritdoc}
   */
  public function toArray() {
    // TODO: Implement toArray() method.
  }

  /**
   * {@inheritdoc}
   */
  public function getTypedData() {
    // TODO: Implement getTypedData() method.
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigDependencyKey() {
    // TODO: Implement getConfigDependencyKey() method.
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigDependencyName() {
    // TODO: Implement getConfigDependencyName() method.
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigTarget() {
    // TODO: Implement getConfigTarget() method.
  }

  /**
   * {@inheritdoc}
   */
  public function addCacheContexts(array $cache_contexts) {
    // TODO: Implement addCacheContexts() method.
  }

  /**
   * {@inheritdoc}
   */
  public function addCacheTags(array $cache_tags) {
    // TODO: Implement addCacheTags() method.
  }

  /**
   * {@inheritdoc}
   */
  public function mergeCacheMaxAge($max_age) {
    // TODO: Implement mergeCacheMaxAge() method.
  }

  /**
   * {@inheritdoc}
   */
  public function addCacheableDependency($other_object) {
    // TODO: Implement addCacheableDependency() method.
  }

}
