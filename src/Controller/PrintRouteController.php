<?php

namespace Drupal\print_route\Controller;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\entity_print\PrintEngineException;
use Drupal\print_route\Entity\PrintableRouteEntity;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class PrintRouteController.
 */
class PrintRouteController extends ControllerBase {

  /**
   * Drupal\Core\Routing\RouteProviderInterface definition.
   *
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected $routerRouteProvider;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The class resolver.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected $classResolver;

  /**
   * Entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepository
   */
  protected $entityRepository;

  /**
   * Renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * The plugin manager for our Print engines.
   *
   * @var \Drupal\entity_print\Plugin\EntityPrintPluginManagerInterface
   */
  protected $printablePluginManager;

  /**
   * The Print builder.
   *
   * @var \Drupal\entity_print\PrintBuilderInterface
   */
  protected $printBuilder;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->routerRouteProvider = $container->get('router.route_provider');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->requestStack = $container->get('request_stack');
    $instance->classResolver = $container->get('class_resolver');
    $instance->entityRepository = $container->get('entity.repository');
    $instance->renderer = $container->get('renderer');
    $instance->printablePluginManager = $container->get('plugin.manager.entity_print.print_engine');
    $instance->printBuilder = $container->get('entity_print.print_builder');
    return $instance;
  }

  /**
   * Validate that the current user has access.
   *
   * We need to validate that the user is allowed to access this route also the
   * print version.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result object.
   */
  public function checkAccess() {
    $params = $this->requestStack->getCurrentRequest()->query;
    if (!$params->get('_routename', FALSE)) {
      return AccessResult::forbidden('No route provided.');
    }

    if ($params->get('_routename', FALSE) == 'print_route.print') {
      // Don't allow recursive printing.
      return AccessResult::forbidden('Recursive printing not allowed.');
    }

    // @todo: Ensure access control checking.
    $account = $this->currentUser();
    // Check if the user has the permission "bypass entity print access".
    $access_result = AccessResult::allowedIfHasPermission($account, 'bypass entity print access');
    if ($access_result->isAllowed()) {
      return $access_result;
    }

    return AccessResult::neutral();
  }

  /**
   * Print route.
   *
   * @return \Symfony\Component\HttpFoundation\Response|array
   *   The response object on error otherwise the Print is sent.
   *
   * @example /print/route?_routename=wrupal_statistics.training_user_statistics&_debug=1&group=14&user=1
   */
  public function printRoute() {
    $content = [];

    // Prepare and execute the route internally.
    $params = $this->requestStack->getCurrentRequest()->query;
    if (!$params->get('_routename', FALSE)) {
      throw new NotFoundHttpException();
    }

    if ($params->get('_routename', FALSE) == 'print_route.print') {
      // Don't allow recursive printing.
      $this->messenger()->addError($this->t("'A 'print route' route can't be printed.'"));
      throw new NotAcceptableHttpException();
    }

    $debugMode = $params->get('_debug', FALSE);

    $printable = PrintableRouteEntity::create([
      'parameters' => $params,
    ]);

    // Create the Print engine plugin.
    $config = $this->config('entity_print.settings');

    try {
      $print_engine = $this->printablePluginManager->createSelectedInstance('pdf');

      if (!$debugMode) {
        return (new StreamedResponse(function () use ($printable, $print_engine, $config) {
          // The printed document is sent straight to the browser.
          $this->printBuilder->deliverPrintable([$printable], $print_engine, $config->get('force_download'), $config->get('default_css'));
        }))->send();
      }
    }
    catch (PrintEngineException $e) {
      // Build a safe markup string using Xss::filter() so that the instructions
      // for installing dependencies can contain quotes.
      $this->messenger()->addError(new FormattableMarkup('Error generating Print: ' . Xss::filter($e->getMessage()), []));
    }

    $use_default_css = $this->config('entity_print.settings')->get('default_css');
    $debug = $this->printBuilder->printHtml($printable, $use_default_css, FALSE);
    $content[] = [
      '#type' => 'markup',
      '#markup' => str_replace('<', '&lt;', $debug),
      '#prefix' => '<pre>',
      '#suffix' => '</pre>',
    ];

    return $content;
  }

}
