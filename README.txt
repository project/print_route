CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

This module allows print custom routes in PDF using entity_print how an API,
this module could fail with some routes like node or views routes (These
routes work ok with entity_print).

The module enable a route to print others in PDF, the path is:
`print/route?_routename=<route name to print>&_debug=<1 | 0>[&<parameter>=<value>...&<parameter N>=<value N>]`

- _routename: The route to print.
- _debug: If the value is 1 display the HTML instead generate a PDF. (Optional)
- [&<parameter>=<value>...&<parameter N>=<value N>]: If the printed route has
  parameters we can add it the URL query.

Example:
To print the user profile page, `/user/1`:
`/print/route?_routename=entity.user.canonical&user=1`

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/print_route

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/print_route

REQUIREMENTS
------------

This module requires the following modules:

 * Entity Print (https://www.drupal.org/project/entity_print)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

The module has no menu or modifiable settings.

MAINTAINERS
-----------

Current maintainer:
 * Pedro Pelaez (psf_) - https://www.drupal.org/u/psf_

This project has been sponsored by:
 * Front ID
   Front ID is a distributed web development company of Drupal experts, but not
   limited to it. Our services include Frontend and Backend development, Drupal
   consultancy and Drupal training.
   We are a small team of six people distributed in four different cities of
   Spain, who has emerged from the community and for the community. We
   collaborate with some of the most important Drupal shops in Spain and Europe
   and our team is well known in the spanish and european community.
